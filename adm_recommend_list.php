<?php
/************************************************
         レコメンド編集（listページ）
         date   @2018-02-28
         author @Keisuke Okuda
         file   @adm_recommend_list.php
/***********************************************/

require_once "../libs/common.php";
require_once "./libs/config.php";
require_once "./libs/function.php";
require_once "Pager/Pager.php";
require_once "vendor/autoload.php";
require "../libs/phpmailer/PHPMailerAutoload.php";
require_once "../libs/phpMailerSend.php";
require ("./libs/class.upload.php");
DBconn();
session_start();
if (!$UsrObj = LoginChk()) {
	//var_dump($_COOKIE);
	//var_dump($UsrObj);
	header("Location: ./login.php");
	exit;
}
// セッションクリア
/*
foreach($_SESSION as $key=>$value) {
	unset($_SESSION[$key]);
}
*/
		// ページ当たりの件数
		$n = 20;
		if (!$_GET[df]) {
			$page_num = 0;
		} else {
			$page_num = $_GET[p];
		}
		if ($_GET[p] == "") {
			$_GET[p] = 0;
		}
		// 検索
		if ($_POST[m] == "search") {
			// セッションに検索条件を格納
			$_SESSION['code'] = $_REQUEST['code'];
			$_SESSION['stm_name'] = $_REQUEST['stm_name'];
			$_SESSION['item_code'] = $_REQUEST['item_code'];
			$_SESSION['item_name'] = $_REQUEST['item_name'];
			$_SESSION['type'] = $_REQUEST['type'];
			$_SESSION['status'] = $_REQUEST['status'];
		}
		// 検索条件クリア
		if ($_GET[m] == "clear") {
		  $_SESSION['code'] = "";
			$_SESSION['stm_name'] = "";
			$_SESSION['item_code'] = "";
			$_SESSION['item_name'] = "";
			$_SESSION['type'] = "";
			$_SESSION['status'] = "";

		}
		// SESSIONに格納されている値を変数に抽象化
			$code = $_SESSION['code'];
			$stm_name = $_SESSION['stm_name'];
			$item_code = $_SESSION['item_code'];
			$item_name = $_SESSION['item_name'];
			$type = $_SESSION['type'];
			$status = $_SESSION['status'];





		// 処理トークン作成
		$memToken = md5(uniqid(rand(), true));
		$_SESSION[mem_sToken] = $memToken;
		//echo "<br>SESSION：";
		//var_dump($_SESSION);

?>
<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="favicon.ico">
		<title><?=TITLE?></title>
		<!-- Bootstrap core CSS -->
		<link href="./css/bootstrap.css" rel="stylesheet">
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="./css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link href="./css/custom.css" rel="stylesheet">
		<link href="./css/bt_pager.css" rel="stylesheet">
		<link href="./css/datepicker_bootstrap.css" rel="stylesheet" type="text/css" />
		<!--<link href="./css/bootstrap-table.css" rel="stylesheet">-->
		<!--<link href="./css/bt_panel.css" rel="stylesheet">-->
		<link href="./css/bootstrap-lumen-modal.css" rel="stylesheet">
		<style>
		.modal-content
		{
			height:600px;
			overflow:auto;
		}
		</style>
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<script src="//use.fontawesome.com/f2d52814d3.js"></script>
		<style>
			.title-h{
				padding: .25em 0 .1em .75em;
				border-left: 6px solid #b0bec5;
				color:#554236;
				font-weight:600;
			}
		</style>
	</head>
	<body class="modal-open">
		<? include("header.php"); ?>
		<div class="container">
			<ul class="breadcrumb" style="margin-top: -65px; margin-bottom: -10px;">
				<li><a href="./">Home</a></li>
				<li class="active">レコメンド更新</li>
				<li class="active">レコメンド管理管理</li>
			</ul>
			<div class="row">
				<div class="col-xs-8 col-xs-offset-2">
					<div class="page-header">
						<blockquote>
							<p>レコメンド管理</p>
							<small>ツクツクホームに掲載するおすすめ商品・店舗を管理します。</small>
						</blockquote>
					</div>
					<form action="<?=$_SERVER[SCRIPT_NAME]?>" method="post">
						<table class="table table-condensed">
							<tbody>
								<tr>
									<td width="120" class="text-right">加盟店番号</td>
									<td width="400">
										<div class="form-group">
											<input class="form-control" name="code" id="code" placeholder="" type="text" value="<?=$code?>">
											<p class="help-block">※部分一致</p>
										</div>
									</td>
									<td width="120" class="text-right">加盟店名称</td>
									<td width="400">
										<div class="form-group">
											<input class="form-control" name="stm_name" id="stm_name" placeholder="" type="text" value="<?=$stm_name?>">
											<p class="help-block">※部分一致</p>
										</div>
									</td>
								</tr>
								<tr>
									<td width="120" class="text-right">商品番号</td>
									<td width="400">
										<div class="form-group">
											<input class="form-control" name="item_code" id="item_code" placeholder="" type="text" value="<?=$item_code?>">
											<p class="help-block">※部分一致</p>
										</div>
									</td>
								</td>
								<td width="120" class="text-right">商品名称</td>
								<td width="400">
									<div class="form-group">
										<input class="form-control" name="item_name" id="item_name" placeholder="" type="text" value="<?=$item_name?>">
										<p class="help-block">※部分一致（例：）</p>
									</div>
								</td>
							</td>
							</tr>
								<tr>
									<td width="120" class="text-right">コンテンツ</td>
									<td width="400">
										<div class="form-group">
											<select name="type" class="form-control">
												<option value="">全てのコンテンツ</option>
												<option value="1"  <? echo ($type == '1' )? "SELECTED" : ""; ?>>通販商品</option>
												<option value="2"  <? echo ($type == '2' )? "SELECTED" : ""; ?>>ウェブチケット</option>
												<option value="3"  <? echo ($type == '3' )? "SELECTED" : ""; ?>>店舗</option>
												<option value="4"  <? echo ($type == '4' )? "SELECTED" : ""; ?>>グルメ</option>
												<option value="5"  <? echo ($type == '5' )? "SELECTED" : ""; ?>>サロン</option>
											</select>
											<p class="help-block">※カテゴリーで検索を行います。</p>
										</div>
									</td>
									<td width="120" class="text-right">ステータス</td>
									<td width="400">
										<div class="form-group">
											<select name="status" class="form-control">
												<option value="">全て</option>
												<option value="1" <? echo ($status == '1')? "SELECTED" : ""; ?>>掲載中</option>
												<option value="2" <? echo ($status == '2')? "SELECTED" : ""; ?>>非掲載</option>
											</select>
											<p class="help-block">※ステータスで検索を行います。</p>
										</div>
									</tr>
							</tbody>
						</table>
						<div class="form-group text-left">
							<button type="submit" class="btn btn-primary" name="m" value="search"><i class="fa fa-search"></i> 検　索</button>
							<button type="button" class="btn btn-default" onclick="location.href='<?=$_SERVER[SCRIPT_NAME]?>?m=clear'"><i class="fa fa-trash fa-lg"></i> クリア</button>
						</div>
					</form>
					<?
					if ($sumsg) {
						?>
						<div class="panel panel-success">
							<div class="panel-heading">
								<h3 class="panel-title">処理結果通知</h3>
							</div>
							<div class="panel-body">
								<?=$sumsg?>
							</div>
						</div>
						<?
					} elseif ($ermsg) {
						?>
						<div class="panel panel-danger">
							<div class="panel-heading">
								<h3 class="panel-title">処理失敗通知</h3>
							</div>
							<div class="panel-body">
								<?=$ermsg?>
							</div>
						</div>
						<?
					}
					if ($_GET[sumsg]) {
						?>
						<div class="panel panel-success">
							<div class="panel-heading">
								<h3 class="panel-title">処理結果通知</h3>
							</div>
							<div class="panel-body">・記事の更新を完了しました。</div>
						</div>
						<?
					}
					if ($_REQUEST[m] == "search") {
						//echo "結果表示";
						if ($page_num <= 0 || $page_num == "") {
							$page_num = 1;
							$offset   = 0;
						} else {
							$page_num = $page_num - 1;
							$offset   = $page_num * $n;
						}
						// 検索結果
						try {
							/*各テーブル名をalias（仮名）
							レコメンド　rcmd_tb, 通販テーブル　onl_shop, ショップテーブル　shop_tb　とする
							 */
							 var_dump($status);
							 var_dump($type);
							$sql = "SELECT
							rcmd_tb.`no`,
							rcmd_tb.`code`,
							rcmd_tb.`type`,
							rcmd_tb.`status` as status,
							rcmd_tb.`regist_date` as regist_date,
							onl_shop.`cl_stm_item_cd` as item_code,
							onl_shop.`item_name` as item_name,
							onl_shop.`item_name_kana` as onl_item_kana,
							shop_tb.`cl_stm_cd` ,
							shop_tb.`cl_stm_name` as shop_name,
							shop_tb.`cl_stm_name` as shop_kana
							FROM
								`recommend_1` rcmd_tb,
								`cl_stm_item_tb` onl_shop,
								`cl_stm_tb` shop_tb
							WHERE
								rcmd_tb.`code` = onl_shop.`cl_stm_item_cd`
							AND
								onl_shop.`cl_stm_cd` = shop_tb.`cl_stm_cd`
							";
							// 検索条件を追加
							// 質問
							if ($code) {
								$sql .= " AND `code` LIKE :code";
							}
							if ($type) {
								$sql .= " AND `type` LIKE :type";
							}
							// ステータス
							if ($status) {
								$sql .= " AND `status` = :status";
							}
							// if ($item_code) {
							// 	$sql .= " AND `rcmd_code` LIKE :code";
							// }
							$obj = $db->prepare($sql);
							// 質問
							if ($code) {
								$obj->bindValue(":code", '%' . addcslashes($code, '\_%') . '%', PDO::PARAM_STR);
							}
							// タグ
							if ($type) {
								$obj->bindValue(":type", $type, PDO::PARAM_INT);
							}
							// ステータス
							if ($status) {
								$obj->bindValue(":status", $status, PDO::PARAM_INT);
							}
							$obj->execute();
							$cnt    = $obj->rowCount();
							$params = array(
								"mode"                  => 'sliding',
								"totalItems"            => $cnt,
								"perPage"               => $n,
								'delta'                 => 2,
								"urlVar"                => "p",
								"extraVars"             => array('df' => 1, 'm' => $_REQUEST['m']),
								"separator"             => '',
								"curPageLinkClassName"  => 'current',
								"prevImg"               => '<i class="fa fa-angle-left"></i>',
								"nextImg"               => '<i class="fa fa-angle-right"></i>',
								"firstPagePre"          => '',
								"firstPagePost"         => '',
								"lastPagePre"           => '',
								"lastPagePost"          => '',
								"spacesBeforeSeparator" => 0,
								"spacesAfterSeparator"  => 0,
							);
							$pager      =& Pager::factory($params);
							$links      = $pager->getLinks();
							$page_range = $pager->getPageRangeByPageId();
							$page_range = range($page_range[0], $page_range[1]);
							$viewLink   = getLink($pager, $page_range, $links);
							?>
							<div class="alert alert-success"><?=$cnt?>件の該当データを表示します。</div>
							<?
							printf ("<div class=\"pager_outer\"><div class=\"pager\" style=\"margin-bottom:40px;\">%s</div></div>\n", $viewLink);
							?>
							<table class="table table-bordered table-hover table-condensed">
								<tbody>
									<tr style="background: #e0e0e0;">
										<td width="5%" style="text-align:  center;">登録番号</td>
										<td width="10%" style="text-align: center;">コンテンツ</td>
										<td width="10%" style="text-align: center;">登録日時</td>
										<td width="10%" style="text-align: center;">加盟店番号</td>
										<td width="20%" style="text-align: center;">加盟店名称</td>
										<td width="10%" style="text-align: center;">商品番号</td>
										<td width="20%" style="text-align: center;">商品名</td>
										<td width="10%" style="text-align: center;">ステータス</td>
									</tr>
									<?
									if ($cnt == 0) {
										?>
										<tr>
											<td colspan="7"><p class="text-center ">該当するデータがありません</p></td>
										</tr>
										<?
									} else {
										// 実データを取得
										$sql .= " ORDER BY `no` DESC";
										$sql .= " LIMIT :n OFFSET :offset";
										$obj = $db->prepare($sql);
										// 質問
										if ($code) {
											$obj->bindValue(":code", '%' . addcslashes($code, '\_%') . '%', PDO::PARAM_STR);
										}
										// タグ
										if ($type) {
											$obj->bindValue(":type", $type, PDO::PARAM_INT);
										}
										// ステータス
										if ($status) {
											$obj->bindValue(":status", $status, PDO::PARAM_INT);
										}
										var_dump($sql);
										$obj->bindValue(":n", $n, PDO::PARAM_INT);
										$obj->bindValue(":offset", $offset, PDO::PARAM_INT);
										$obj->execute();
										$i = 1;
										while($row = $obj->fetch(PDO::FETCH_ASSOC)) {
											//$item_codeの値に応じて値を変数$cate_name_1に代入
											switch ($row[type]) {
												case "1": //会員登録
												$contents = "会員登録";
												break;
												case "2": //お買い物
												$contents = "お買い物";
												break;
												case "3": //ポイント
												$contents = "ポイント";
												break;
												case "4": //マイページ
												$contents = "マイページ";
												break;
												case "5": //メルマガ
												$contents = "メルマガ";
												break;
												case "6": //その他
												$contents = "その他";
												break;
											}

											// ステータス変換
											switch ($row[status]) {
												case '1':	// 掲載中
													$status = "<span class=\"label label-success\">有効</span>";
												break;
												case '2':	// 非掲載
													$status = "<span class=\"label label-danger\">無効</span>";
												break;
											}
											var_dump($status);
											?>
											<tr>
												<td style="text-align: center;"><a data-toggle="modal" href="usr_faq_detail.php?no=<?=$row[no]?>" data-target="#<?=$row[no]?>"><?=$row[no]?></a></td>
												<td style="text-align: center;"><?=$contents?></td>
												<td style="text-align: center;"><?=$row[regist_date]?></td>
												<td style="text-align: center;"><h5><?=$row[cl_stm_cd]?></h5></td>
												<td style="text-align: center;"><h5><?=$row[shop_name]?></h5></td>
												<td style="text-align: center;"><h5><?=$row[item_code]?></h5></td>
												<td style="text-align: center;"><h5><?=$row[item_name]?></h5></td>
												<td style="text-align: center;"><h5><?=$status?></h5></td>
												<!-- <td style="text-align: center;"><a class="btn btn-primary btn-xs" href="usr_faq_edit.php?no=<?=$row[no]?>">編　集</a></td> -->
												<!--詳細モーダル-->
												<div class="modal fade" id="<?=$row[no]?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
													<div class="modal-dialog modal-lg">
														<div class="modal-content"></div>
													</div>
												</div>
											</tr>
											<?
										}
									}
									?>
								</tbody>
							</table>
							<?
						} catch (PDOException $e) {
							die($e->getMessage());
							echo "クエリエラー（検索時）";
						}
						printf ("<div class=\"pager_outer\"><div class=\"pager\" style=\"margin-bottom:40px;\">%s</div></div>\n", $viewLink);
					}
					?>
				</div>
			</div>
		</div>
		<? include("footer.php"); ?>
		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<!--<script src="./js/bootstrap.min.js"></script>-->
		<script src="js/jquery-2.1.0.min.js"></script>
		<script src="js/bootstrap.min.3.1.1.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="./js/ie10-viewport-bug-workaround.js"></script>
	</body>
</html>
<?php
// DB切断
$db = null;
?>
