<?php
/************************************************
         代理店サイトお知らせ記事作成（投稿ページ）
         date   @2018-02-19
         author @Keisuke Okuda
         file   @adm_recommend_reg.php
/***********************************************/


require_once "../libs/common.php";
require_once "./libs/config.php";
require_once "./libs/function.php";
require_once "Pager/Pager.php";
require_once "vendor/autoload.php";
require "../libs/phpmailer/PHPMailerAutoload.php";
require_once "../libs/phpMailerSend.php";
require ("./libs/class.upload.php");
DBconn();
session_start();
if (!$UsrObj = LoginChk()) {
	//var_dump($_COOKIE);
	//var_dump($UsrObj);
	header("Location: ./login.php");
	exit;
}
// セッションクリア
/*
foreach($_SESSION as $key=>$value) {
	unset($_SESSION[$key]);
}
*/

// 登録処理開始
if ($_POST[reg]) {
	$memToken = isset($_POST[memToken])? $_POST[memToken] : "";
	$memSave = isset($_SESSION[mem_sToken])? $_SESSION[mem_sToken] : "";
	if ($memToken !== "") {
		if ($memToken === $memSave) {
			unset($_SESSION[mem_sToken]);
			session_write_close();
			session_start();


			//各$_POSTの値を変数に代入
			$code = $_POST[code];
			$type = $_POST[type];
			$status = $_POST[status];




		   switch($type){
		    case "1": // 通販,
		    //通販テーブル,
		    $table = "`cl_stm_item_tb`";
		    //通販テーブルコード
		    $table_code =	"`cl_stm_item_cd` = "."'$code'";
		    //通販テーブルの検索条件
		    $condition = "`gloval_category_1` != 16";
		    //通販テーブルにデータがなかった場合のエラーメッセージ
		    $code_exist_err = "・入力された商品コードに該当する通販の商品が見つかりません。<br>";
				//option value 用
				$selected_1_1 = "selected=\"selected\"";
				$selected_1_2 = "";
				$selected_1_3 = "";
				$selected_1_4 = "";
				$selected_1_5 = "";
		   	break;
		   	case "2": // ウェブチケット
		   	//ウェブチケットテーブル
		   	$table = "`cl_stm_item_tb`";
		   	//ウェブチケットテーブルのコード
		   	$table_code =	"`cl_stm_item_cd` = "."'$code'";
		   	//ウェブチケットテーブルの検索条件
		   	$condition = "`gloval_category_1` = 16";
		   	//ウェブチケットテーブルにデータがなかった場合のエラーメッセージ
		   	$code_exist_err = "・入力された商品コードに該当するウェブチケットの商品が見つかりません。<br>";
				//option value 用
				$selected_1_1 = "";
				$selected_1_2 = "selected=\"selected\"";
				$selected_1_3 = "";
				$selected_1_4 = "";
				$selected_1_5 = "";
		   	break;
		   	case "3": // ショップページ,ホーム
		   	//ショップページ,ホームテーブル
		   	$table = "`cl_stm_tb`";
		   	//ショップページ,ホームコード
		   	$table_code =	"`cl_stm_cd` = "."'$code'";
		   	//ショップページ,ホームテーブルの検索条件
		   	$condition = "`publish_flg` = 0";
		   	//ショップページ,ホームテーブルにデータがなかった場合のエラーメッセージ
		   	$code_exist_err = "・入力された店舗コードに該当する店舗が見つかりません。<br>";
				//option value 用
				$selected_1_1 = "";
				$selected_1_2 = "";
				$selected_1_3 = "selected=\"selected\"";
				$selected_1_4 = "";
				$selected_1_5 = "";
		   	break;
		   	case "4": // ショップページ,グルメ
		   	//ショップページ,グルメテーブル
		   	$table = "`cl_stm_tb`";
		   	//ショップページ,グルメテーブルのコード
		   	$table_code =	"`cl_stm_cd` = "."'$code'";
		   	//ショップページ,グルメテーブルの検索条件
		   	$condition = "`publish_flg` = 0 AND `m04` = 1 AND `gurumet_flg` = 1";
		   	//ショップページ,グルメテーブルにデータがなかった場合のエラーメッセージ
		   	$code_exist_err = "・入力された店舗コードに該当する店舗が見つかりません。<br>";
				//option value 用
				$selected_1_1 = "";
				$selected_1_2 = "";
				$selected_1_3 = "";
				$selected_1_4 = "selected=\"selected\"";
				$selected_1_5 = "";
		   	break;
		   	case "5": // ショップページ,ビューティー
		   	//ショップページ,ビューティーテーブル
		   	$table = "`cl_stm_tb`";
		   	//ショップページ,ビューティーテーブルのコード
		   	$table_code =	"`cl_stm_cd` = "."'$code'";
		   	//ショップページ,ビューティーテーブルの検索条件
		   	$condition = "`publish_flg` = 0 AND `m06` = 1 AND `beauty_flg` = 1";
		   	//ショップページ,グルメテーブルにデータがなかった場合のエラーメッセージ
		   	$code_exist_err = "・入力された店舗コードに該当する店舗が見つかりません。<br>";
				//option value 用
				$selected_1_1 = "";
				$selected_1_2 = "";
				$selected_1_3 = "";
				$selected_1_4 = "";
				$selected_1_5 = "selected=\"selected\"";
		   	break;
		   }




			  $sql = "SELECT COUNT(*) FROM $table
			  WHERE $table_code AND `status` = 0 AND $condition";



			  $set =  $db->query($sql);
			  $column_count = $set->fetchColumn();

			  if($column_count == 0){
			    $ermsg .= $code_exist_err;
			  }




			//記事タイトルと本文いずれかが未入力の場合はエラー処理
			if (!$_POST[code]) {
				$ermsg .= "・コードが入力されていません。<br>";
			}
			if (!$_POST[type]) {
				$ermsg .= "・カテゴリーが入力されていません。<br>";
			}

			//$ermsgがfalseの場合
			if (!$ermsg) {
				try {
					$sql2 = "INSERT INTO `recommend_1` (
					`code`,
					`type`,
					`regist_date`,
					`status`
					)
					VALUES (
					:code,
					:type,
					NOW(),
					:status
					)";
					//sql実行
					$obj = $db->prepare($sql2);
					$obj->bindValue(":code", $code, PDO::PARAM_STR);
					$obj->bindValue(":type", $type, PDO::PARAM_INT);
					$obj->bindValue(":status", 1, PDO::PARAM_INT);
					$obj->execute();
					unset($_POST);
					$sumsg = "・レコメンド登録を完了しました。";
				} catch (PDOException $e) {
					die($e->getMessage());
					echo "クエリエラー（記事登録時）";
				}
			}
		}
	}
	$_POST[reg] = "";
}

// 処理トークン作成
$memToken = md5(uniqid(rand(), true));
$_SESSION[mem_sToken] = $memToken;
//echo "<br>SESSION：";
//var_dump($_SESSION);

?>
<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="favicon.ico">
		<title><?=TITLE?></title>
		<!-- Bootstrap core CSS -->
		<link href="./css/bootstrap.css" rel="stylesheet">
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="./css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link href="./css/custom.css" rel="stylesheet">
		<link href="./css/bt_pager.css" rel="stylesheet">
		<link href="./css/datepicker_bootstrap.css" rel="stylesheet" type="text/css" />
		<!--<link href="./css/bootstrap-table.css" rel="stylesheet">-->
		<!--<link href="./css/bt_panel.css" rel="stylesheet">-->
		<link href="./css/bootstrap-lumen-modal.css" rel="stylesheet">
		<style>
		.modal-content
		{
			height:600px;
			overflow:auto;
		}
		</style>
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<script src="//use.fontawesome.com/f2d52814d3.js"></script>
		<style>
			.title-h{
				padding: .25em 0 .1em .75em;
				border-left: 6px solid #b0bec5;
				color:#554236;
				font-weight:600;
			}
		</style>
	</head>
	<body class="modal-open">
		<? include("header.php"); ?>
		<div class="container">
			<ul class="breadcrumb" style="margin-top: -65px; margin-bottom: -10px;">
				<li><a href="./">Home</a></li>
				<li class="active">記事更新管理</li>
				<li class="active">レコメンド登録</li>
			</ul>
			<div class="row">
				<div class="col-xs-8 col-xs-offset-2">
					<div class="page-header">
						<blockquote>
							<p>レコメンド登録</p>
							<small>ツクツクホームに掲載するおすすめ商品・店舗を登録します。</small>
						</blockquote>
					</div>
					<?
					if ($sumsg) {
						?>
						<div class="panel panel-success">
							<div class="panel-heading">
								<h3 class="panel-title">処理結果通知</h3>
							</div>
							<div class="panel-body">
								<?=$sumsg?>
							</div>
						</div>
						<?
					} elseif ($ermsg) {
						?>
						<div class="panel panel-danger">
							<div class="panel-heading">
								<h3 class="panel-title">処理失敗通知</h3>
							</div>
							<div class="panel-body">
								<?=$ermsg?>
							</div>
						</div>
						<?
					}
					?>
					<form action="<?=$_SERVER[SCRIPT_NAME]?>" method="post">
						<input type="hidden" name="action" value="1">
						<input type="hidden" name="memToken" value="<?=$memToken?>">
						<table class="table table-condensed">
							<tbody>
								<tr>
									<td width="120" class="text-right">コード</td>
									<td width="400">
										<input class="form-control" type="text" id="code" name="code" value="<?=($_POST[code]) ? $_POST[code] : "" ?>" maxlength="128">
										<p class="help-block">※商品コード・店舗コードを入力してください。</p>
									</td>
									<td width="120" class="text-right">カテゴリー</td>
									<td width="400">
										<select name="type" class="form-control">
											<option value="1" <?=$selected_1_1?>>通販商品</option>
											<option value="2" <?=$selected_1_2?>>ウェブチケット</option>
											<option value="3" <?=$selected_1_3?>>ショップ</option>
											<option value="4" <?=$selected_1_4?>>グルメ</option>
											<option value="5" <?=$selected_1_5?>>サロン</option>
										</select>
										<p class="help-block">※記事のカテゴリーを選択してください。</p>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="text-right">
							<button type="submit" class="btn btn-primary" name="reg" value="1">登　　録</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<? include("footer.php"); ?>
		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<!--<script src="./js/bootstrap.min.js"></script>-->
		<script src="js/jquery-2.1.0.min.js"></script>
		<script src="js/bootstrap.min.3.1.1.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="./js/ie10-viewport-bug-workaround.js"></script>
	</body>
</html>
<?php
// DB切断
$db = null;
?>
